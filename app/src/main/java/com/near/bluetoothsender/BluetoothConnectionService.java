package com.near.bluetoothsender;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

public class BluetoothConnectionService {
    private static final String TAG= "BluetoothConnection";

    private static final String appname = "bluetoothsender";

    private static final UUID myUUID = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    private final BluetoothAdapter mBluetoothAdapter;
    Context context;
    private AcceptThread mInsecureAcceptThread;


    public BluetoothConnectionService(Context context) {
        context = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    private class AcceptThread extends Thread{
        private final BluetoothServerSocket mServerSocket;
        public AcceptThread(){
            BluetoothServerSocket tmp = null;

            try {
                tmp= mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(appname,myUUID);
                Log.d(TAG, "AcceptThraed : Setting up Server using: " +myUUID);
            }catch (IOException se){

            }
            mServerSocket = tmp;
        }

        public void run(){
            Log.d(TAG,"run: AcceptThread running.");
            BluetoothSocket socket = null;

            try {
                Log.d(TAG,"run: RFCOM server socket start....");
                socket = mServerSocket.accept();
                Log.d(TAG,"run: RFCOM server socket accepted connection.");
            } catch (IOException E){
                Log.d(TAG, "AcceptThraed : IOException " +E.getMessage());

            }

            if (socket != null){

            }

        }
    }
}
